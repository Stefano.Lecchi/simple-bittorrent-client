package main

import(
	"simple-bittorrent-client/src/go/file_decoder"
	"C"
	"unsafe"
	"log"
	"bufio"
	"os"
	"strings"
	"strconv"
)

// Constants.
const EOF = "EOF"
const ANNOUNCE = "Announce"
const LENGTH = "Length"
const PIECELENGTH = "PieceLength"
const NAME = "Name"
const PIECEHASHES = "PieceHashes"
const HASHESNUMBER = "HashesNumber"

func SearchData(dataToSearch string) string {

	// Read the file "decodedInfo.txt" line by line, until you find
	// the label the string `dataToSearch` refers to, then return the next line.
	file, err := os.Open("decodedInfo.txt")
	file_decoder.CheckErr(err)

	reader := bufio.NewReader(file)
	var line string
	var trimmedString string

	for {
		line, err = reader.ReadString('\n')
		trimmedString = strings.TrimSpace(line)

		if trimmedString == EOF {
			break
		}

		if trimmedString == dataToSearch {
			line, err = reader.ReadString('\n')
			trimmedString = strings.TrimSpace(line)
			break
		}
	}

	return trimmedString
}

/*
	These functions are ment to be exported to be used by c++.
*/

//export ReadAnnounce
func ReadAnnounce() *C.char {

	return C.CString(SearchData(ANNOUNCE))
}

//export ReadLength
func ReadLength() int {

	length, err := strconv.Atoi(SearchData(LENGTH))
	if err != nil {
		return -1
	}

	return length
}

//export ReadPieceLength
func ReadPieceLength() int {

	length, err := strconv.Atoi(SearchData(PIECELENGTH))
	if err != nil {
		return -1
	}

	return length
}

//export ReadName
func ReadName() *C.char {

	return C.CString(SearchData(NAME))
}

//export ReadNumberOfHashes
func ReadNumberOfHashes() int {

	length, err := strconv.Atoi(SearchData(HASHESNUMBER))
	if err != nil {
		return -1
	}

	return length
}

//export ReadHashes
func ReadHashes() []*C.char {

	// Read the file "decodedInfo.txt" line by line, until you find
	// the label "PieceHashes" refers to, then read all the lines until you reach "EOF".
	file, err := os.Open("decodedInfo.txt")
	file_decoder.CheckErr(err)

	
	reader := bufio.NewReader(file)
	var line string
	var trimmedString string

	// Retrieving the number of hashes.
	hashesNumber := ReadNumberOfHashes()

	// Making up the slice to return to C:
	// Unsafe pointer to the array
	cArray := C.malloc(C.size_t(hashesNumber * 20))
	arrayUnsPtr := unsafe.Pointer(cArray)

	// Converting the unsafe pointer to a pointer of type *[1 << 30]C.YourType
	arrayPtr := (*[1 << 30]*C.char)(arrayUnsPtr)

	// Finally declaring the slice.
	hashes := arrayPtr[:hashesNumber :hashesNumber]
	
	for {
		line, err = reader.ReadString('\n')
		trimmedString = strings.TrimSpace(line)

		if trimmedString == EOF {
			break
		}

		if trimmedString == PIECEHASHES {
			
			for i:= 0; i < hashesNumber; i++ {
				bytes, err := reader.ReadSlice('\n')
				file_decoder.CheckErr(err)
				hashes [i] = C.CString(string(bytes[:]))
			}

			break
		}
	}

	return hashes
}

func main() {
	
	inPath := os.Args[1]

	tf, err := file_decoder.ParseBencode(inPath)
	if err != nil {
		log.Fatal(err)
	}
	tf.WriteToFile()
}