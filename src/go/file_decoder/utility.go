package file_decoder

// Utility function to check errors.
func CheckErr(e error) {
	if e != nil {
		panic(e)
	}
}