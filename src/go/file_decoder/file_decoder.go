package file_decoder

import(
	"bytes"
	"fmt"
	"crypto/sha1"
	"os"
	"strconv"

	"github.com/jackpal/bencode-go"
)

/*
	This is utility code to get the torrent file in a readable shape.
	The major reason to go with Go is because at the moment the focus is not on the 
	".torrent" file parser but on the client itself, and there is a very well written Bencode
	library for Go which will make things easy to us.
*/

type torrentFile struct {

	Announce string
	InfoHash [20] byte
	PieceHashes [][20] byte
	PieceLength int
	Length int
	Name string
}

type bencodeInfo struct {

	Pieces string `bencode:"pieces"`
	PieceLength int `bencode:"piece length"`
	Length int `bencode:"length"`
	Name string `bencode:"name"`
}

type bencodeTorrent struct {

	Announce string `bencode:"announce"`
	Info bencodeInfo `bencode:"info"`
}

func (decInfo *torrentFile) WriteToFile() {

	// Create the file.
	var filePath = "decodedInfo.txt"
	file, err := os.Create(filePath)
	CheckErr(err)

	// Announce.
	fmt.Fprintln(file, "Announce")
	_, err = file.WriteString(decInfo.Announce)
	CheckErr(err)
	fmt.Fprintln(file, "")
	
	// InfoHash.
	fmt.Fprintln(file, "InfoHash")
	_, err = file.Write(decInfo.InfoHash[:])
	CheckErr(err)
	fmt.Fprintln(file, "")
	
	// Piece length.
	fmt.Fprintln(file, "PieceLength")
	temp := strconv.Itoa(decInfo.PieceLength)
	_, err = file.WriteString(temp)
	CheckErr(err)
	fmt.Fprintln(file, "")

	// Length.
	fmt.Fprintln(file, "Length")
	temp = strconv.Itoa(decInfo.Length)
	_, err = file.WriteString(temp)
	CheckErr(err)
	fmt.Fprintln(file, "")

	// Name.
	fmt.Fprintln(file, "Name")
	_, err = file.WriteString(decInfo.Name)
	CheckErr(err)
	fmt.Fprintln(file, "")

	//PieceHashes (and the number representing how many there are).
	fmt.Fprintln(file, "HashesNumber")
	temp = strconv.Itoa(len(decInfo.PieceHashes))
	_, err = file.WriteString(temp)
	fmt.Fprintln(file, "")

	fmt.Fprintln(file, "PieceHashes")
	for i := 0; i < len(decInfo.PieceHashes); i++ {
		_, err = file.Write(decInfo.PieceHashes[i][:])
		CheckErr(err)
		fmt.Fprintln(file, "")
	}

	fmt.Fprintln(file, "EOF")

	// Closing the file.
	err = file.Close()
	CheckErr(err)
}

func ParseBencode(path string) (torrentFile, error) {

	file, err := os.Open(path)
	if err != nil {
		return torrentFile{}, err
	}
	defer file.Close()

	benTorr := bencodeTorrent{}
	err = bencode.Unmarshal(file, &benTorr)
	if err != nil {
		return torrentFile{}, err
	}

	return benTorr.ToTorrentFile()
}

func (info *bencodeInfo) Hash() ([20] byte, error) {

	var buffer bytes.Buffer

	err := bencode.Marshal(&buffer, *info)
	if err != nil {
		return [20] byte{}, err
	}

	hash := sha1.Sum(buffer.Bytes())
	return hash, nil
}

func (info *bencodeInfo) SpliHashes() ([][20]byte, error) {

	// Length of sha1 hash
	hashLength := 20
	buffer := []byte (info.Pieces)

	if len(buffer) % hashLength != 0 {
		err := fmt.Errorf("Received malformed pieces if length %d", len(buffer))
		return nil, err
	}

	hashesNumber := len(buffer) / hashLength
	hashes := make([][20] byte, hashesNumber)

	for i := 0; i < hashesNumber; i ++ {
		copy(hashes[i][:], buffer[i * hashLength : (i + 1) * hashLength])
	}
	return hashes, nil
}

func (bto *bencodeTorrent) ToTorrentFile() (torrentFile, error) {

	infoHash, err := bto.Info.Hash()
	if err != nil {
		return torrentFile{}, err
	}

	pieceHashes, err := bto.Info.SpliHashes()
	if err != nil {
		return torrentFile{}, err
	}

	parsedTorrent := torrentFile {
		Announce : bto.Announce,
		InfoHash : infoHash,
		PieceHashes : pieceHashes,
		PieceLength : bto.Info.PieceLength,
		Length : bto.Info.Length,
		Name : bto.Info.Name,
	}
	return parsedTorrent, nil
}
