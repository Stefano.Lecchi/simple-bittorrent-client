# Simple BitTorrent Client

Simple BitTorrent client made using both Go and C++.

The reason behind using both Go and C++ is that I found a very well-made Go library to completly parse the .torrent file, and the focus 
is on the actual client, not on the parsing of the file. Could be interesting implementing the parser in c++ later.